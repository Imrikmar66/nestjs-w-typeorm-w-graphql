import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Artist } from "./artist/artist.entity";
import { Style } from './style/style.entity';

export default {
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: '',
    database: 'festival',
    entities: [Artist, Style],
    synchronize: true,
} as TypeOrmModuleOptions;