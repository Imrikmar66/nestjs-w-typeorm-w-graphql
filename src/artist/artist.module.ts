import { Module } from '@nestjs/common';
import { ArtistService } from './artist.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Artist } from './artist.entity';
import { ArtistResolver } from './artist.resolver';

@Module({
    imports: [TypeOrmModule.forFeature([Artist]),],
    providers: [ArtistService, ArtistResolver,]
})
export class ArtistModule {}
