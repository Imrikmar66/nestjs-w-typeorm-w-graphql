import { Injectable } from '@nestjs/common';
import { Artist } from './artist.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ArtistService {

    constructor(
        @InjectRepository(Artist)
        private readonly artistRepository: Repository<Artist>,
    ){}

    all() {
        return this.artistRepository.find();
    }

    find( id: number ) {
        return this.artistRepository.findOne( id );
    }

    styles( id: number ) {
        return this.artistRepository.findOne({ where: {id}, relations: ["styles"] });
    }

}
