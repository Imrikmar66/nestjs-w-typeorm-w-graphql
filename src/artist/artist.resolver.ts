import { Resolver, Query, Args, ResolveProperty, Parent } from "@nestjs/graphql";
import { ArtistService } from "./artist.service";
import { Artist } from "./artist.entity";

@Resolver('Artist')
export class ArtistResolver {

    constructor(
        private readonly artistService: ArtistService
    ) {}

    @Query()
    async artists() {
        return await this.artistService.all();
    }

    @Query()
    async artist( @Args('id') id: number ) {
        return await this.artistService.find( id );
    }

    @ResolveProperty()
    async styles(@Parent() artist: Artist ) {
        return await artist.styles;
    }

}