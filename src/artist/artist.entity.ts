import { Column, Entity, PrimaryGeneratedColumn, ManyToMany, JoinTable } from 'typeorm';
import { Style } from '../style/style.entity';

@Entity()
export class Artist {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 100 })
    name: string;

    @Column('text')
    description: string;

    @ManyToMany(type => Style)
    @JoinTable()
    styles: Promise<Style[]>;

}
