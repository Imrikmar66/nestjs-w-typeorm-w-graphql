import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArtistModule } from './artist/artist.module';
import { StyleModule } from './style/style.module';
import graphqlconfig from './graphql.config';
import ormconfig from './orm.config';


@Module({
    imports: [
        GraphQLModule.forRoot(graphqlconfig),
        TypeOrmModule.forRoot( ormconfig ),
        ArtistModule,
        StyleModule,
    ],
})
export class ApplicationModule {}
