import { join } from "path";

export default {
    typePaths: ['./**/*.graphql'],
    definitions: {
        path: join(process.cwd(), 'src/graphql.ts'),
    },
    outputAs: 'class',
    watch: true
}