import { Resolver, Query } from '@nestjs/graphql';
import { StyleService } from './style.service';

@Resolver('Style')
export class StyleResolver {

    constructor(
        private readonly styleService: StyleService
    ) {}

}
