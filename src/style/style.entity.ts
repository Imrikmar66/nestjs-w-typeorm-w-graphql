import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Style {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 100 })
    label: string;
}
