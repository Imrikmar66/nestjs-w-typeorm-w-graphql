import { Injectable } from '@nestjs/common';
import { Style } from './style.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class StyleService {

    constructor(
        @InjectRepository(Style)
        private readonly styleRepository: Repository<Style>,
    ){}

    all() {
        return this.styleRepository.find();
    }

}
