import { Module } from '@nestjs/common';
import { StyleService } from './style.service';
import { StyleResolver } from './style.resolver';
import { Style } from './style.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
    imports: [TypeOrmModule.forFeature([Style]),],
    providers: [StyleService, StyleResolver]
})
export class StyleModule {}
