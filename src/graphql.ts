
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
export interface Artist {
    id: string;
    name: string;
    description: string;
    styles: Style[];
}

export interface IQuery {
    artists(): Artist[] | Promise<Artist[]>;
    artist(id: string): Artist | Promise<Artist>;
}

export interface Style {
    id: string;
    label: string;
}
